#include <iostream>
#include <string>
#include <queue>
#include <mutex>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"


using namespace std;

TEST_CASE("if with initializer")
{
    vector<int> vec = { 6, 23, 645, 23, 665, 23534, 3453, 12 };

    SECTION("Before Cpp17")
    {
        auto pos = std::find(vec.begin(), vec.end(), 665);

        if (pos != vec.end())
        {
            cout << "Item is found: " << (*pos) << endl;
        }
    }

    SECTION("Since Cpp17")
    {
        if (auto pos = std::find(vec.begin(), vec.end(), 665); pos != vec.end())
        {
            cout << "Item is found: " << (*pos) << endl;
        }
    }

    SECTION("use case with mutex")
    {
        queue<int> q;
        mutex mtx_q;

        int value;

        SECTION("Before Cpp17")
        {
            {
                lock_guard<mutex> lk{mtx_q};
                if (!q.empty())
                {
                    value = q.front();
                    q.pop();
                }
            }

            //...
        }

        SECTION("Since Cpp17")
        {
            if (lock_guard lk{mtx_q}; !q.empty())
            {
                value = q.front();
                q.pop();
            }
        }
    }
}

enum class Status
{
    bad, on, off
};


class Gadget
{
    Status status_;

public:
    Gadget() : status_{Status::off}
    {}

    Status get_status()
    {
        return status_;
    }
};



TEST_CASE("switch with initializer")
{
    switch(auto g = Gadget{}; g.get_status())
    {
        case Status::on:
            cout << "Device is on" << endl;
            break;
        case Status::off:
            cout << "Device is off" << endl;
            break;
        default:
            cout << "Error occured" << endl;
    }
}

#include <iostream>
#include <string>
#include <optional>
#include <charconv>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

TEST_CASE("optional")
{
    optional<int> opt_int;

    REQUIRE(opt_int.has_value() == false);

    opt_int = 5;

    REQUIRE(opt_int.has_value());

    optional opt_str = "text"s;

    SECTION("getting to value")
    {
        auto& value = *opt_str;
        REQUIRE(value == "text"s);

        REQUIRE(opt_str->size() == 4);

        auto hold_value = opt_str.value();
        REQUIRE(hold_value == "text"s);
    }

    SECTION("for empty opional")
    {
        optional<int> opt_int2;

        SECTION("* and -> for empty optional give UB")
        {
            //*opt_int2 = 7; // UB
        }

        SECTION("value() throws exception")
        {
            REQUIRE_THROWS_AS(opt_int2.value(), std::bad_optional_access);
        }
    }

    SECTION("value_or_default")
    {
        optional<int> opt_int;

        auto result = opt_int.value_or(42);
        REQUIRE(result == 42);

        opt_int = 665;
        REQUIRE(opt_int.value_or(13) == 665);

    }
}

optional<int> as_int(string_view sv)
{
    int result{};
    auto [str_buffer, err_code] = std::from_chars(sv.begin(), sv.end(), result);

    if (err_code == std::errc{0})
        return result;
    else
        return std::nullopt;
}

unique_ptr<int> as_int_with_up(string_view sv)
{
    int result{};
    auto [str_buffer, err_code] = std::from_chars(sv.begin(), sv.end(), result);

    if (err_code == std::errc{0})
        return make_unique<int>(result);
    else
        return nullptr;
}

TEST_CASE("as_int")
{
    SECTION("happy path")
    {
        string str = "42";

        auto result = as_int(str);

        REQUIRE(result.value() == 42);
    }
}

#include <iostream>
#include <string>
#include <array>
#include <thread>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

TEST_CASE("constexpr lambda expressions")
{
    auto square = [](const auto& x) { return x * x; };

    SECTION("implicit constexpr")
    {
        array<int, square(8)> arr1 = {};

        static_assert(arr1.size() == 64);
    }

    SECTION("explicit constexpr")
    {
        constexpr array<int, square(8)> values = { 1, 2, 3, 4, 5, 6 , 7};

        auto sum = [](const auto& data) constexpr {
            long result{};
            for (auto it = begin(data); it != end(data); ++it)
                result += *it;
            return result;
        };

        static_assert(sum(values) == 28);
    }
}

class Gadget
{
    int counter_{};
public:
    Gadget()
    {
        cout << "Gadget(" << this << ")" << endl;
    }

    Gadget(const Gadget& g) : counter_{g.counter_}
    {
        cout << "Gadget(cc: " << this << " from " << &g << ")" << endl;
    }

    Gadget(Gadget&& g) : counter_{g.counter_}
    {
        g.counter_ = 0;
        cout << "Gadget(mv: " << this << " from " << &g << ")" << endl;
    }

    ~Gadget()
    {
        cout << "~Gadget(" << this << ")" << endl;
    }

    void play()
    {
        ++counter_;
    }

    auto report()
    {
        return [*this] { cout << "Gadget(" << this << " - counter: " << counter_ << ")" << endl; };
    }
};

TEST_CASE("using capture with *thread")
{
    thread thd;

    {
        Gadget g;
        g.play();
        g.play();

        thd = thread{g.report()};
    }
    cout << "----" << endl;

    thd.join();
}

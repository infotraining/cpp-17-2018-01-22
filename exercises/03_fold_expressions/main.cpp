#include <iostream>
#include <set>
#include <string>
#include <vector>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

template <typename Container, typename... Ts>
size_t matches(const Container& c, Ts... value)
{
    return (... + count(begin(c), end(c), value));
}

TEST_CASE("matches - returns how many items is stored in a container")
{
    vector<int> v{1, 2, 3, 4, 5};

    REQUIRE(matches(v, 2, 5) == 2);
    REQUIRE(matches(v, 100, 200) == 0);
    REQUIRE(matches("abcdef", 'x', 'y', 'z') == 0);
    REQUIRE(matches("abcdef", 'a', 'd', 'f') == 3);
}

template <typename Container, typename... Ts>
void push_all(Container& c, Ts&&... value)
{
    (c.push_back(std::forward<Ts>(value)), ...);
}

TEST_CASE("push_all - multiple push_backs")
{
    using namespace Catch::Matchers;

    std::vector<int> v{1, 2, 3};
    push_all(v, 4, 5, 6);

    REQUIRE_THAT(v, Equals(vector{1, 2, 3, 4, 5, 6}));
}

template<typename Set, typename... Ts>
bool insert_all(Set& s, Ts&&... key)
{
    return (... && s.insert(std::forward<Ts>(key)).second);
}

TEST_CASE("insert_all - performs inserts and returns true if all have been succesful")
{
    set<int> my_set = {1, 2, 3};

    REQUIRE(insert_all(my_set, 4, 5, 6) == true);
    REQUIRE(insert_all(my_set, 6, 3) == false);
}

template <typename T, typename... Ts>
bool within(const T& low, const T& high, const Ts&... value)
{
    auto is_within = [&low, &high](const T& v) { return v >= low && v <= high; };

    return (... && is_within(value));
}

TEST_CASE("within - checks if all values fit in range [low, high]")
{
    REQUIRE(within( 10,  20,  1, 15, 30) == false);
    REQUIRE(within( 10,  20,  11, 12, 13) == true);
    REQUIRE(within(5.0, 5.5,  5.1, 5.2, 5.3) == true);
}

/////////////////////////////////////////////
//

template <typename... Bases>
struct Derived : Bases...
{
   void print()
   {
       (..., Bases::print());
   }
};

struct A
{
    void print()
    {
        cout << "A" << endl;
    }
};

struct B
{
    void print()
    {
        cout << "B" << endl;
    }
};

using MultiBaseStruct = Derived<A, B>;

TEST_CASE("multibase")
{
    MultiBaseStruct mbs;

    mbs.print();
}

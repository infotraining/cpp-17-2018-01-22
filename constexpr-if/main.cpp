#include <iostream>
#include <string>
#include <type_traits>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"


using namespace std;

template <typename T>
std::string as_string(T x)
{
    if constexpr(std::is_same_v<T, std::string>)
    {
        return x;
    }
    else if constexpr(std::is_arithmetic_v<T>)
    {
        return std::to_string(x);
    }
    else
    return std::string{x};
}

TEST_CASE("as_string with compile-time if")
{
    SECTION("strings")
    {
        string txt = "hello";

        auto result = as_string(txt);

        REQUIRE(result == "hello");
    }

    SECTION("numbers")
    {
        int x = 42;
        auto result = as_string(x);

        REQUIRE(result == "42"s);
    }

    SECTION("floating points")
    {
        auto result = as_string(3.14);

        REQUIRE(result == "3.140000");
    }

    SECTION("c-strings")
    {
        const char* c_txt = "text";
        REQUIRE(as_string(c_txt) == "text"s);
    }
}


template <typename T>
void process_array(const T&  tab)
{
    if constexpr(std::size(tab) <= 255)
            cout << "processing small table" << endl;
    else
    cout << "processing large table" << endl;
}

TEST_CASE("processing arrays")
{
    int tab1[42] = {};
    process_array(tab1);

    int tab2[1024] = {};
    process_array(tab2);

    array<int, 2048> arr;
    process_array(arr);
}


///////////////////////////////////////////

//void print()
//{}

template <typename Head, typename... Tail>
void print(Head head, Tail... tail)
{
    cout << head << endl;

    if constexpr(sizeof...(tail) > 0)
            print(tail...);
}

TEST_CASE("constexpr if with variadic templates")
{
    print(3, 3.14, "hello"s, "world");
}

///////////////////////
/// EXTRA
///

template<typename Iterator, typename Distance>
void advance(Iterator& pos, Distance n) {
    using cat = std::iterator_traits<Iterator>::iterator_category;

    if constexpr (std::is_same_v<cat, std::random_access_iterator_tag>) {
        pos += n;
    }
    else if constexpr (std::is_same_v<cat,
                       std::bidirectional_access_iterator_tag>) {
        if (n >= 0) {
            while (n--) {
                ++pos;
            }
        }
        else {
            while (n++) {
                --pos;
            }
        }
    }
    else { // input_iterator_tag
        while (n--) {
            ++pos;
        }
    }
}

struct Foo
{
    string foo()
    {
        return "text";
    }
};

template <typename T>
void bar(T obj)
{
    if constexpr(auto value = obj.foo(); is_same_v<string, decltype(value))
    {
        //...
    }
}

TEST_CASE("constexpr if with initializer")
{

}

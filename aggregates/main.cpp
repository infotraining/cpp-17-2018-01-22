#include <iostream>
#include <string>
#include <complex>
#include <type_traits>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"


using namespace std;

namespace BeforeCpp17
{
    struct CData
    {
        int a;
        double b;
        int tab[3];
    };
};

struct Derived : BeforeCpp17::CData
{
    int c;
    string text;
};

namespace Explain
{
    template <typename T, size_t N>
    struct array
    {
        T items[N];

        // no constructors
    };
}

TEST_CASE("std::array is an aggregate")
{
    std::array<int, 10> arr1 = { { 1, 2, 3, 4 } };
}

TEST_CASE("agregates in cpp17 can derive from base class")
{
    using namespace BeforeCpp17;

    CData cd1{ 3, 3.14, { 1, 2, 3} };

    Derived d{ { 3, 3.14, {1 ,2, 3 } }, 42, "text" };
}

template <typename T>
struct Aggregate : std::string, std::complex<T>
{
    string data;
};

TEST_CASE("complex aggregate")
{
    //Aggregate<double> agg1{ {"text"}, {4.5, 3.4}, "test"};

    static_assert(is_aggregate_v<Aggregate<double>>);
}

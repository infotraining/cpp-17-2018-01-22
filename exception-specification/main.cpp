#include <iostream>
#include <string>
#include <string_view>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"


using namespace std;

void f1() {}
void f2() noexcept {}

TEST_CASE("exception handling spec is part of the type")
{
    //void (*ptr_f1)() noexcept = &f1; // ERROR
    void (*ptr_f2)() noexcept = &f2; // OK
}

///////////////////////////////////////////////////////////
//

class Base
{
public:
    virtual void foo() noexcept
    {}

    virtual ~Base() = default;
};

class Derived : public Base
{
public:
    void foo() noexcept override  // noexcept is mandatory
    {}
};

//////////////////////////////////////////////////////////////////////////
//

template <typename F>
void call(F f1, F f2)
{
    f1();
    f2();
}

TEST_CASE("")
{
    call(f1, f1); // OK
    call(f2, f2); // OK
    //call(f1, f2); // ERROR - deduction conflict
}

#include <iostream>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

namespace Cpp17
{

template <size_t N>
struct Arg
{
    template <typename T, typename... Ts>
    constexpr auto operator()(T arg, Ts... args) const
    {
        if constexpr(N > 0)
        {
            return Arg<N-1>{}(args...);
        }
        else
        {
            return arg;
        }
    }
};

template <size_t N>
constexpr auto arg = Arg<N>{};

}

TEST_CASE("constexpr-if with variadic templates")
{
    SECTION("c++17 implementation")
    {
        using namespace Cpp17;

        static_assert(Arg<0>{}(0, 1, 2, 3, 4) == 0, "Error");
        static_assert(Arg<1>{}(0, 1, 2, 3, 4) == 1, "Error");
        static_assert(Arg<2>{}(0, 1, 2, 3, 4) == 2, "Error");
        static_assert(Arg<3>{}(0, 1, 2, 3, 4) == 3, "Error");
        static_assert(Arg<4>{}(0, 1, 2, 3, 4) == 4, "Error");


        static_assert(arg<1>(0, 1, 2, 3, 4) == 1);
        static_assert(arg<2>(0, 1, 2, 3, 4) == 2);
        static_assert(arg<3>(0, 1, 2, 3, 4) == 3);
        static_assert(arg<4>(0, 1, 2, 3, 4) == 4);
    }
}

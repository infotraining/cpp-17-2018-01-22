#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <tuple>
#include <memory>
#include <functional>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

template <typename T1, typename T2, typename T3 = T2>
struct Data1
{
    T1 v1;
    T2 v2;
    T3 v3;

    Data1(T1 arg1, T2 arg2 = T2{}, T3 arg3 = T3{})
        : v1{arg1}, v2{arg2}, v3{arg3}
    {}
};

template <typename GuessType, typename T>
void assert_deduction(T obj)
{
    static_assert(is_same_v<GuessType, decltype(obj)>);
}

void foo(int x)
{}

TEST_CASE("auto deduction")
{
    SECTION("1st case")
    {
        auto x = 42; // int

        SECTION("references and const(volatile) modifiers are removed ")
        {
            const int cx = 42;
            auto c = cx; // int

            int& ref_x = x;
            auto y = ref_x; // int

            const int& cref_x = cx;
            auto z = cref_x; // int
        }

        SECTION("function and arrays decay")
        {
            int tab[10];
            auto x = tab; // int*

            const int ctab[3] = {1 , 2, 3};
            auto cx = ctab; // const int*

            auto fp = foo; // void(*)(int)
        }
    }

    SECTION("2nd case - auto with &")
    {
        auto x = 42; // int

        SECTION("references and const(volatile) modifiers are preserved")
        {
            const int cx = 42;
            auto& cr = cx;  //const int&

            int& ref_x = x;
            auto& y = ref_x; // int&

            const int& cref_x = x;
            auto& z = cref_x; // const int&
        }

        SECTION("function and arrays do not decay")
        {
            int tab[10];
            auto& x = tab; // int(&)[10]

            const int ctab[3] = {1 , 2, 3};
            auto& cx = ctab; // const int(&)[3]

            auto fp = foo; // void(&)(int)
        }
    }

    SECTION("3rd case - auto&&")
    {
        int x = 10;
        const int y = 10;
        const int& ry = y;

        auto&& ux = x; // int&
        auto&& uy = y; // const int&
        auto&& ury = ry; // const int&
        auto&& str = "text"s; // string&&

        auto&& it = str.begin(); // std::string::iterator&&
    }
}

TEST_CASE("simple deduction for a class with a constructor")
{
    Data1 d1{1, 3.14, "text"};
    assert_deduction<Data1<int, double, const char*>>(d1);

    Data1 d2{3.14f, "hello"s};
    assert_deduction<Data1<float, string, string>>(d2);

    int tab[10] = {};
    const int(&ref_tab)[10] = tab;

    Data1 d3(foo, tab, ref_tab);
    assert_deduction<Data1<void(*)(int), int*, const int*>>(d3);
}

template <typename T>
struct Data2
{
    T value;

    Data2(const T& v) : value(v)
    {}

    template <typename U>
    Data2(initializer_list<U> items) : value{items}
    {}
};

// deduction guide
template <typename T> Data2(T) -> Data2<T>;

Data2(const char*) -> Data2<string>;

template <typename T> Data2(initializer_list<T>) -> Data2<vector<T>>;

TEST_CASE("deducing from constructor with ref")
{
    int x = 10;
    const int& rx = x;

    Data2 d1(rx);
    assert_deduction<Data2<int>>(d1);

    Data2 d1b(d1); // special case - copy of d1
    assert_deduction<Data2<int>>(d1b);

    Data2 d2(foo);
    assert_deduction<Data2<void(*)(int)>>(d2);

    int tab[3] = { 1, 2, 3 };
    Data2 d3(tab);
    assert_deduction<Data2<int*>>(d3);

    Data2 d4("text");
    assert_deduction<Data2<string>>(d4);

    Data2 d5{1, 2, 3, 4, 5};
    assert_deduction<Data2<vector<int>>>(d5);
}

/////////////////////////////////////////////////////

template <typename T>
struct Complex
{
    T real, img;

    Complex(const T& r, const T& i) : real{r}, img{i}
    {}
};

template <typename T1, typename T2>
Complex(T1, T2) -> Complex<std::common_type_t<T1, T2>>;

TEST_CASE("Complex with deduction guides")
{
    Complex c1(5, 10);
    assert_deduction<Complex<int>>(c1);

    Complex c2(5.1, 6.5);
    assert_deduction<Complex<double>>(c2);

    Complex c3 = c2;
    assert_deduction<Complex<double>>(c3);

    Complex c4(5, 3.14);
    assert_deduction<Complex<double>>(c4);
}


///////////////////////////////////////////////////

template <typename T1, typename T2>
struct Aggregate1
{
    T1 value1;
    T2 value2;
};

template <typename T1, typename T2> Aggregate1(T1, T2) -> Aggregate1<T1, T2>;


template <typename T>
struct Aggregate2
{
    T value;
};

template <typename T> Aggregate2(T) -> Aggregate2<T>;

TEST_CASE("aggregates need explicit deduction guide")
{
    Aggregate1 agg1{7, "text"s};
    assert_deduction<Aggregate1<int, string>>(agg1);

    Aggregate2 agg2{6};
    assert_deduction<Aggregate2<int>>(agg2);
}

/////////////////////////////////////////////////////////////
//

template <typename T, size_t N>
struct Array
{
    T items[N];

    using iterator = T*;

    constexpr size_t size() const
    {
        return N;
    }

    constexpr iterator begin()
    {
        return items;
    }

    constexpr iterator end()
    {
        return items + N;
    }
};

//template <typename T, typename... Ts>
//Array(T, Ts...) -> Array<common_type_t<T, Ts...>, sizeof...(Ts) + 1>;

template <typename T, typename... Ts>
Array(T, Ts...) -> Array<enable_if_t<(is_same_v<T, Ts> && ...), T>, sizeof...(Ts) + 1>;

TEST_CASE("array with deduction guide")
{
    array<int, 4> arr1 = { 1, 2, 3, 4 };
    array arr2 = { 1, 2, 3, 4 };

    Array arr3 {1, 2, 3, 4 };
    assert_deduction<Array<int, 4>>(arr3);

    Array arr4{1.8, 3.4};
    assert_deduction<Array<double, 2>>(arr4);
}


///////////////////////////////////////////////////////////////
//

template <typename T>
class Container
{
    vector<T> data;

public:
    Container(size_t n, const T& value) : data(n, value)
    {
        cout << "Container(size_t, const T&)" << endl;
    }

    template <typename Iterator, typename = typename iterator_traits<Iterator>::value_type>
    Container(Iterator first, Iterator last) : data(first, last)
    {
        cout << "Container(Iterator, Iterator)" << endl;
    }
};

template <typename Iterator>
Container(Iterator, Iterator) -> Container<typename iterator_traits<Iterator>::value_type>;

TEST_CASE("constructor with iterators")
{
    list<int> lst { 1, 2, 3, 4 };

    Container container(lst.begin(), lst.end());
    assert_deduction<Container<int>>(container);

    Container<int> data(10, -1);
}

///////////////////////////////////////////////////////////////////
//  std library deduction guides
//

TEST_CASE("pair")
{
    pair p1{1, 3.14};
    assert_deduction<pair<int, double>>(p1);

    pair p2{3.14f, "text"};
    assert_deduction<pair<float, const char*>>(p2);

    int tab[3] = {1, 2, 3};
    pair p3{"tab"s, tab};
    assert_deduction<pair<string, int*>>(p3);
}

TEST_CASE("tuple")
{
    tuple t1{1, 3.14, "text"s, "text"};
    assert_deduction<tuple<int, double, string, const char*>>(t1);

    int x = 10;
    const int& crx = x;

    tuple t2{&x, crx};
    assert_deduction<tuple<int*, int>>(t2);
}

TEST_CASE("smart_ptrs")
{
    unique_ptr<int> uptr{new int(3)}; // no deduction for unique_ptr and smart_ptr

    shared_ptr sptr = move(uptr);
    sptr = make_unique<int>(42);

    weak_ptr wptr = sptr;

    shared_ptr sptr2{wptr};
}

TEST_CASE("function")
{
    function f = [](int x, int y) { return x * y; };
    REQUIRE(f(3, 4) == 12);
}

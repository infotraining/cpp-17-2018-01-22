#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"


using namespace std;

class CopyMoveDisabled
{
public:
    int value;
    CopyMoveDisabled(int value) : value{value} {}
    CopyMoveDisabled(const CopyMoveDisabled&) = delete;
    CopyMoveDisabled(CopyMoveDisabled&&) = delete;
};

CopyMoveDisabled copy_elided()
{
    return CopyMoveDisabled{42};
}

void copy_elided(CopyMoveDisabled arg)
{
    cout << "arg: " << arg.value << endl;
}

TEST_CASE("Copy elision for return")
{
    CopyMoveDisabled cmd = copy_elided();

    REQUIRE(cmd.value == 42);    
}

TEST_CASE("Copy elision for args")
{
    copy_elided(CopyMoveDisabled{665});
}

#include <iostream>
#include <string>
#include <tuple>
#include <memory>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

class Customer
{
private:
    std::string first_;
    std::string last_;
    u_int8_t age_;

public:
    Customer(std::string f, std::string l, long v)
        : first_(std::move(f))
        , last_(std::move(l))
        , age_(v)
    {
    }

    std::string first() const
    {
        return first_;
    }

    std::string last() const
    {
        return last_;
    }

    long age() const
    {
        return age_;
    }
};

template <>
struct std::tuple_size<Customer>
{
    static const size_t value = 3;
};

template <>
struct std::tuple_element<2, Customer>
{
    using type = uint8_t;
};

template <size_t Idx>
struct std::tuple_element<Idx, Customer>
{
    using type = std::string;
};

template <size_t Idx>
auto get(const Customer&);

template <>
auto get<0>(const Customer& c)
{
    return c.first();
}

template <>
auto get<1>(const Customer& c)
{
    return c.last();
}

template <>
auto get<2>(const Customer& c)
{
    return c.age();
}

TEST_CASE("")
{
    Customer c("Jan", "Kowalski", 42);

    auto[f, l, a] = c;

    REQUIRE(f == "Jan");
    REQUIRE(l == "Kowalski");
    REQUIRE(a == 42);
}

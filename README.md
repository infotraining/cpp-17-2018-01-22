# README #

### Proxy settings ###

* add to .profile

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

### Ankieta ###

* https://docs.google.com/forms/d/e/1FAIpQLSeuL9U8VjR8-tcHjeaKo3ibZGtOotyU3gu7iCR-qDMIJMzrtw/viewform?hl=pl

### Energy efficiency ###

* http://greenlab.di.uminho.pt/wp-content/uploads/2017/09/paperSLE.pdf
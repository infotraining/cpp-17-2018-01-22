#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    template <typename Arg>
    Arg sum(Arg arg)
    {
        return arg;
    }

    template <typename Arg, typename... Args>
    Arg sum(Arg arg, Args... args)
    {
        return arg + sum(args...);
    }
}

template <typename... Args>
auto fold_sum(Args... args)
{
    return (... + args); // left fold
}

template <typename... Args>
auto foldr_sum(Args... args)
{
    return (args + ...); // right fold
}

TEST_CASE("sum")
{
    SECTION("before cpp17")
    {
        using namespace BeforeCpp17;

        auto result = sum(1, 2, 3, 4, 5);
        REQUIRE(result == 15);

        auto result2 = sum(1);
    }

    SECTION("since cpp17")
    {
        auto result = fold_sum(1, 2, 3, 4, 5);
        REQUIRE(result == 15);

        REQUIRE(foldr_sum(1, 2, 3, 4, 5) == 15);

        SECTION("for strings")
        {
            REQUIRE(fold_sum("one"s, "two", "three") == "onetwothree"s);

            REQUIRE(foldr_sum("one", "two", "three"s) == "onetwothree"s);
        }
    }
}

template <typename... Args>
bool all_true(Args... args)
{
    return (... && args);
}

TEST_CASE("all_true")
{
    REQUIRE(all_true(1, 1, 1, 1, true) == true);
}


template<typename... Args>
void print(Args... args)
{
    (cout << ... << args) << "\n";
}

TEST_CASE("print all")
{
    print(1, 3.14, "text"s);
}

////////////////////////////////////////////////
//
//

struct Window {
void show() { std::cout << "showing Window\n"; }
};

struct Widget {
void show() { std::cout << "showing Widget\n"; }
};

struct Toolbar {
void show(){ std::cout << "showing Toolbar\n"; }
};

TEST_CASE("calling show")
{
    Window wnd;
    Widget wdgt;
    Toolbar tlbr;

    auto printer = [](auto&&... args) { (std::forward<decltype(args)>(args).show(), ...); };

    printer(wnd, wdgt, tlbr);
}

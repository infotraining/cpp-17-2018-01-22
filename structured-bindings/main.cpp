#include <iostream>
#include <string>
#include <map>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <tuple>
#include <algorithm>
#include <numeric>

using namespace std;

namespace BeforeCpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        auto min_max_pair = std::minmax_element(data.begin(), data.end());

        double avg = std::accumulate(data.begin(), data.end(), 0.0) / data.size();

        return make_tuple(*min_max_pair.first, *min_max_pair.second, avg);
    }
}

namespace SinceCpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        auto [min, max] = std::minmax_element(data.begin(), data.end());

        double avg = std::accumulate(data.begin(), data.end(), 0.0) / data.size();

        return make_tuple(*min, *max, avg);
    }
}

static int coordinates[3] = { 1, 2, 3 };

auto get_point() -> int(&)[3]
{
    return coordinates;
}

TEST_CASE("structured bindings")
{
    SECTION("for tuples")
    {
        vector<int> data = { 1, 2, 3, 4, 5, 6 };

        SECTION("before cpp17")
        {
            int min, max;
            double avg;
            tie(min, max, ignore) = BeforeCpp17::calc_stats(data);

            REQUIRE(min == 1);
            REQUIRE(max == 6);
        }

        SECTION("in cpp17")
        {
           auto [min, max, avg] = BeforeCpp17::calc_stats(data);

           REQUIRE(min == 1);
           REQUIRE(max == 6);
        }
    }

    SECTION("for structures")
    {
        struct Data
        {
            int n;
            char c;
            std::string txt;
        };

        SECTION("structured binding for struct")
        {
            Data d1{ 1, 'a', "text" };

            [[ maybe_unused ]] auto [counter, character, str] = d1;

            counter = 42;

            REQUIRE(d1.n == 1);
        }

        SECTION("structured bindings with ref")
        {
            Data d1{1, 'a', "text"};

            [[ maybe_unused ]] auto& [counter_ref, c_ref, str_ref] = d1;

            counter_ref = 42;

            REQUIRE(d1.n == 42);
        }

        SECTION("structured bindings with alignas")
        {
             Data d1{1, 'a', "text"};

             alignas(16) auto [counter, character, str] = d1;
        }
    }

    SECTION("for arrays")
    {
        auto [x, y, z] = get_point();

        REQUIRE(x == 1);
        REQUIRE(y == 2);
        REQUIRE(z == 3);
    }
}

TEST_CASE("how it works")
{
    struct Time
    {
        int h, m;
    };

    Time t1{10, 18};

    auto [hours, minutes] = t1;

    SECTION("is interpreted as")
    {
        auto temp = t1;
        auto& hours = temp.h;
        auto& minutes = temp.m;
    }
}


TEST_CASE("USE CASES")
{
    map<int, string> dictionary = { { 1, "one"}, {5, "five"}, {3, "three"} };

    SECTION("iteration over map")
    {
        SECTION("Before Cpp17")
        {
            for(const auto& item : dictionary)
            {
                cout << item.first << " - " << item.second << endl;
            }
        }

        SECTION("SinceCpp17")
        {
            for(const auto& [key, value] : dictionary)
                cout << key << " - " << value << endl;
        }
    }

    SECTION("insert to asociative container")
    {
        SECTION("Before Cpp17")
        {
            auto result = dictionary.insert(make_pair(2, "two"));

            if (result.second)
            {
                cout << "Item " << (result.first->first) << " has been inserted" << endl;
            }
        }

        SECTION("Since Cpp17")
        {
            if (auto [where, is_success] = dictionary.insert(pair(2, "two")); is_success)
            {
                const auto& [key, value] = *where;
                cout << "Item " << value << " has been inserted" << endl;
            }
        }
    }
}

enum Something
{};

template<>
struct std::tuple_size<Something>
{
    static unsigned int const value = 2;
};

template <>
struct std::tuple_element<0, Something>
{
    using type = int;
};

template <>
struct std::tuple_element<1, Something>
{
    using type = double;
};

template <size_t>
auto get(Something);

template <>
auto get<0>(Something)
{
    return 42;
}

template <>
auto get<1>(Something)
{
    return 3.14;
}

TEST_CASE("structured binding wiht tuple-like item")
{
    auto [x, y] = Something();

    REQUIRE(x == 42);
    REQUIRE(y == Approx(3.14));
}

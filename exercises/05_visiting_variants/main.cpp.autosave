#include <iostream>
#include <string>
#include <variant>
#include <vector>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

struct Circle
{
    int radius;
};

struct Rectangle
{
    int width, height;
};

struct Square
{
    int size;
};

struct AreaVisitor
{
    double operator()(const Circle& c) const
    {
        return 3.14 * c.radius * c.radius;
    }

    double operator()(const Rectangle& r) const
    {
        return r.width * r.height;
    }

    double operator()(const Square& s) const
    {
        return s.size * s.size;
    }
};

template<typename... Ts>
struct Overloader : Ts...
{
    using Ts::operator()...; // new feature C++17
};

template <typename... Ts>
Overloader(Ts...) -> Overloader<Ts...>;

TEST_CASE("visit a shape variant and calculate area")
{
    using Shape = variant<Circle, Rectangle, Square>;

    vector<Shape> shapes = {Circle{1}, Square{10}, Rectangle{10, 1}};

    double total_area{};

    for(const auto& shp : shapes)
    {
        // static visitation
        // total_area += std::visit(AreaVisitor{}, shp);

        // visting variant in-place
        auto area_visitor = Overloader {
                [](const Circle& c) { return c.radius * c.radius * 3.14; },
                [](const Rectangle& r) -> double { return r.width * r.height; },
                [](const Square& s) -> double { return s.size * s.size; }
        };

        total_area += std::visit(area_visitor, shp);
    }

    REQUIRE(total_area == Approx(113.14));
}

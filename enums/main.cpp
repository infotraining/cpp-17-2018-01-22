#include <iostream>
#include <string>
#include <cstddef>
#include <bitset>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"


using namespace std;

enum class Coffee { espresso, latte, dopio };

//TEST_CASE("enums class - direct list initialization")
//{
//    Coffee c{1};

//    //Coffee c2 = 1;
//    //Coffee c3(1);

//    c = Coffee{0};

//    REQUIRE(c == Coffee::latte);
//}


//enum State : uint8_t { on, off };

//TEST_CASE("enum - direct list initialization")
//{
//    State s{1};
//}

//TEST_CASE("std::byte")
//{
//    std::byte b1{32};
//    std::byte b2{0x3F};
//    std::byte b3{0b1110'0000};

//    std::bitset<32> bs1("0101010101");

//    int value_b3 = std::to_integer<int>(b3);

//    auto b4 = (b1>>2) & b2;

//    std::byte byte_array[4] { b1, b2, b3, std::byte{8} };

//    static_assert(sizeof(std::byte) == 1);
//}


TEST_CASE("auto direct list init rule")
{
    SECTION("Before Cpp17")
    {
        int x1{42};
        auto x2{42}; // initializer_list<int>
        //auto x3{45, 665}; // initializer_list<int>
        auto x4 = {42}; // // initializer_list<int>
        auto x5 = {42, 665}; // // initializer_list<int>}
    }

    SECTION("Since Cpp17")
    {
        auto x2{42}; // int
        //auto x3{45, 665}; // ERROR
        auto x4 = {45, 665}; // initializer_list
    }

}
